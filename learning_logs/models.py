from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):
    """Um assunto sobre o qual o usuario esta aprendendo"""
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User)
    # Esse método é como um ToString do Java.
    def __str__(self):
        """Devolve uma representacao em string do modelo"""
        return self.text


class Entry(models.Model):
    """Algo especifico aprendido sobre o assunto"""
    topic = models.ForeignKey(Topic)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    # Meta armazena informações extras para administrar o modelo
    class Meta:
        verbose_name_plural = 'entires'
        # Esse método é como um ToString do Java.
        def __str__(self):
            """Devolve uma representacao em string do modelo"""
            return self.text[:50] + "..."
